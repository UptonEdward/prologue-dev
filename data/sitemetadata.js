const siteMetadata = {
  title: "Prologue 序章",
  author: "槐序",
  authorDesc:"自我批判和反叛思考",
  publishName: "Prologue",
  headerTitle: "What's past is prologue",
  description:
    "对当下的反思和批判",
  language: "zh",
  keywords:['prologue', '序言'],
  siteUrl: "https://prologue.dev",
  siteRepo: "prologue.dev",
  repoid:"R_kgDOHl4HGg",
  siteLogo: "/static/favicons/favicon.ico",
  image: "/static/images/avatar.png",
  cover: "/static/images/cover2.jpg",
  email: "prologue@outlook.sg",
  github: "huaixuOvO",
  twitter: "@huaixuOvO",
  twitterid:"1507965040603074564",
};

module.exports = siteMetadata;
