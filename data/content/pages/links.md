---
title: 友情链接
description: 但愿十年后的某天，这些链接仍存活，与各位作者共勉。
---

## 友链

[Rercel](https://rercel.com/) - 哲学片段与学术之路

[yihtseu](https://blog.yizhou.info/) - 博学之，审问之，慎思之，明辨之，笃行之。

## 说明

如有意互换友链，可以在[Github使用markdown编辑](https://github.com/huaixuOvO/prologue.dev/edit/master/data/content/pages/links.md)添加然后Pull Request，也可以通过以下评论告知，若得知会尽快添加。
